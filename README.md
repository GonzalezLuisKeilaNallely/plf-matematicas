# Conjuntos, Aplicaciones y funciones (2002)

Mapa conceptual con PlantUML
```plantuml
@startmindmap
+[#lightseagreen] Razonamiento matemático
++[#lightskyblue] Conjunto
+++[#lightslategray] Tipos
++++[#lightsteelblue] Universal
+++++[#skyblue] Contiene todos los elementos
++++++_ es decir
+++++++[#silver] Conjunto de conjuntos planteados.
++++[#lightsteelblue] Vacio
+++++[#skyblue] Conjunto sin elementos
+++[#lightslategray] Operaciones
++++[#lightsteelblue] Complementación
+++++[#skyblue] Elementos que no pertenecen al conjunto
++++[#lightsteelblue] Intersección
+++++[#skyblue] Elementos que pertenecen a ambos conjuntos
++++[#lightsteelblue] Unión
+++++[#skyblue] Elementos pertenecientes al menos a uno de los conjuntos
++++[#lightsteelblue] Diferencia
+++++[#skyblue] Elementos solo presentes en uno de los conjuntos
+++[#lightslategray] Cardinalidad
++++[#skyblue] Numero de elementos del conjunto
+++[#lightslategray] Representación gráfica
++++_ por medio de 
+++++[#skyblue] Diagramas de Venn
++[#lightskyblue] Aplicación
+++_ es una
++++[#lightslategray] Transformación
+++++_ de todos los
++++++[#skyblue] Elementos de un conjunto
+++++++[#silver] A un solo elemento
++++++++_ De otro conjunto
+++[#lightslategray] Tipos
++++[#skyblue] Inyectiva
++++[#skyblue] Suprayectiva
++++[#skyblue] Biyectiva
++[#lightskyblue] Función
+++[#lightslategray] Aplicación entre conjuntos de numeros
++++_ donde para un
+++++[#skyblue] Elemento de conjunto numérico
++++++_ le corresponde
+++++++[#silver] Un unico elemento de otro conjunto numérico
@endmindmap
```

Enlace de recurso:

[Temas 6 y 7: Conjuntos, Aplicaciones y funciones @ Canal UNED](https://canal.uned.es/video/5a6f1b77b1111f15098b4609)

# Funciones (2010)

Mapa conceptual con PlantUML
```plantuml
@startmindmap
+[#tomato] Funciones
++_ Es una
+++[#coral] Aplicación entre conjuntos de numeros
++++[#salmon] Correspondencia de elementos
+++++[#pink] Un elemento único del otro conjunto
++++_ Se representa mediante un
+++++[#salmon] Plano cartesiano
++++++_ Donde
+++++++[#pink] Se toma un eje para cada conjunto de números
++[#coral] Tipos
+++[#salmon] Creciente
++++[#pink] Su imagen aumenta al aumentar el valor en x 
+++[#salmon] Decreciente
++++[#pink] Disminuye su imagen al aumentar su valor en x 
+++[#salmon] Máximo
++++[#pink] Valor más alto del intervalo en una funcion
+++[#salmon] Mínimo
++++[#pink] Valor más bajo del intervalo en una función
+++[#salmon] Límite
++++[#pink] Aproximación de x más cercana sin alcanzar la función
+++[#salmon] Derivada
++++[#pink] Razón de cambio de una función
@endmindmap
```
Enlace de recurso:

[Funciones @ Canal UNED](https://canal.uned.es/video/5a6f2d09b1111f21778b457f)

# La matemática del computador (2002)

Mapa conceptual con PlantUML

```plantuml
@startmindmap
+[#mediumseagreen] Matemática del ordenador
++_ se basa en
+++[#limegreen] La aritmética finita
++++_ emplea
+++++[#lawngreen] Cifras significativas
++++++[#yellowgreen] Que aportan información sin ambigüedad
++++++_ emplean los procesos
+++++++[#yellowgreen] Redondeo
++++++++[#greenyellow] Despreciar cifras no escenciales
+++++++++[#lightgreen] Retocando la ultima cifra
+++++++[#yellowgreen] Truncamiento
++++++++[#greenyellow] Cortar el número a determinadas cifras
++++_ generando
+++++[#lawngreen] Problemas y errores
++++++_ consecuencia de la
+++++++[#yellowgreen] Aproximación
++++++++_ que expresa
+++++++++[#greenyellow] Lo alejado del resultado real
++++_ por medio de 
+++++[#lawngreen] Codificación
++++++_ se trabaja con
+++++++[#yellowgreen] Aritmetica binaria
++++++++_ expresando
+++++++++[#greenyellow] Presencia o ausencia de corriente
++++++++_ utiliza
+++++++++[#greenyellow] Sistemas numéricos
++++++++++_ para
+++++++++++[#lightgreen] Compactar el número de digitos de un número
@endmindmap
```

Enlace de recurso:

[La matemática del computador. El examen final @ Canal UNED](https://canal.uned.es/video/5a6f1b76b1111f15098b45fa)

[Tema 12: Las matemáticas de los computadores. El examen final  @ Canal UNED](https://canal.uned.es/video/5a6f1b81b1111f15098b4640)